CREATE TABLE usuario
(
    id serial PRIMARY KEY,
    nombreCompleto text NOT NULL,
    edad integer NOT NULL
);

insert into usuario (nombreCompleto, edad) values( 'Juan alberto canqui ticona', 35 )
