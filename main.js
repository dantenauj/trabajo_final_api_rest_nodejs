const express = require("express");
const bodyParser = require("body-parser");
const pg = require("pg");
const { query } = require("express");
////dpg-cf1cbp6n6mpkr6apt510-a.oregon-postgres.render.com

const config = {
      user:'todos_db_aqxy_user',
      database:'todos_db_aqxy',
      password:'Z6UvVqEAxGPXodiVSeWnaBxUbhS2izIZ',
      host:'dpg-cf1cbp6n6mpkr6apt510-a.oregon-postgres.render.com',
      port:5432,
      ssl:true,
      idleTimeoutMillis: 30000
}
const client = new pg.Pool(config);

// Modelo
class TodoModel {
  constructor() {
    this.status = { nameSystem: 'api-users', version: '0.0.1', developer: 'Juan Alberto Canqui Ticona' , email: 'juan.alberto.canqui.ticona@gmail.com'};
  }
  
 async getUsuarios(){
    const rest = await client.query('select * from usuario;');
    return rest.rows;
  }

  async addUsuario(nombreCompleto, edad) {
    const  query = 'insert into usuario(nombreCompleto, edad) values ($1, $2) returning *';
    const value = [   nombreCompleto, edad];
    const rest = await client.query(query, value);
    return rest;
  }

  async getUsuariosPromedio(){
    const rest = await client.query('select sum(edad)/count(*)::numeric  as promedioEdad from usuario;');
    return rest.rows;
  }

  async editUsuario(index, nombreCompleto, edad) {
    const  query = 'update usuario set nombrecompleto = $1, edad = $2  where id = $3 returning *';
    const value = [   nombreCompleto, edad, index ];
    const rest = await client.query(query, value);
    return rest;
  }

  async deleteUsuario(index) {
       const  query = 'delete from usuario  where id = $1 returning *';
    const value = [index];
    const rest = await client.query(query, value);
    return rest;
  }

}

// Controlador
class TodoController {
  constructor(model) {
    this.model = model;
  }

  async getUsuarios() {
   return await this.model.getUsuarios();
  }
  
  async addUsuario(nombreCompleto, edad) {
   await this.model.addUsuario(nombreCompleto, edad);
  }

  async getUsuariosPromedio() {
    return await this.model.getUsuariosPromedio();
   }

   async editUsuario(index, nombreCompleto, edad){
    await this.model.editUsuario(index, nombreCompleto, edad);
   }

   async deleteUsuario(index) {
    await this.model.deleteUsuario(index);
  }
}

// Vistas (Rutas)
const app = express();
const todoModel = new TodoModel();
const todoController = new TodoController(todoModel);

app.use(bodyParser.json());

app.get("/usuarios", async ( req, res) => {
  const response = await todoController.getUsuarios();
  res.json(response);
});
///--- Listar Usuarios ---\\\
app.get("/usuarios", async ( req, res) => {
  const response = await todoController.getUsuarios();
  res.json(response);
});
///--- Registrar Usuario ---\\\
app.post("/usuarios", (req, res) => {
  const nombreCompleto = req.body.nombreCompleto;
  const edad = req.body.edad;
  todoController.addUsuario(nombreCompleto, edad);
  res.sendStatus(200);
});
///--- Promedio de Usuario ---\\\
app.get("/promedio-edad", async ( req, res) => {
  const response = await todoController.getUsuariosPromedio();
  res.json(response);
});
///--- informacion del api-rest ---\\\
app.get("/status", (req, res) => {
  res.send(todoController.model.status);
});
///--- editar Usuario ---\\\
app.put("/usuarios/:index", (req, res) => {
  const index = req.params.index;
  const nombreCompleto = req.body.nombreCompleto;
  const edad = req.body.edad;
  todoController.editUsuario(index, nombreCompleto, edad);
  res.sendStatus(200);
});
///--- eliminar Usuario ---\\\
app.delete("/usuarios/:index", (req, res) => {
  const index = req.params.index;
  todoController.deleteUsuario(index);
  res.sendStatus(200);
});

app.listen(3000, () => {
  console.log("Server listening on port 3000");
});
